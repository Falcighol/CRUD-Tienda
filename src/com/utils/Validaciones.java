package com.utils;

import java.awt.event.KeyEvent;

/**
 * Nombre de la clase: Validaciones
 * Version: 1.0
 * Fecha: 2/8/2018
 * Copyright: Juan Pablo Elias
 * @author Juan Pablo Elias Hernández
 */
public class Validaciones
{
    public Validaciones() {
    }
    
    public void numbersOnly(KeyEvent evt) {
        if (!Character.isDigit(evt.getKeyChar())) {
            evt.consume();
        }
    }
    
    public void numbersAndPointOnly(KeyEvent evt, String val) {
        if (!Character.isDigit(evt.getKeyChar()) && (evt.getKeyChar() != '.' || val.contains("."))) {
            evt.consume();
        }
    }

    public void wordsOnly(KeyEvent evt) {
        if (!Character.isLetter(evt.getKeyChar()) && evt.getKeyChar() != KeyEvent.VK_SPACE) {
            evt.consume();
        }
    }

    public boolean IsNullOrEmpty(String val) {

        if (val.equals("")) {
            return true;
        }
        if (val.length() == 0) {
            return true;
        }
        if (val == null) {
            return true;
        }
        return false;
    }
}
