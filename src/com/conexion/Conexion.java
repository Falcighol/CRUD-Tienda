package com.conexion;

import java.sql.*;
import javax.swing.JOptionPane;

/**
 * Nombre de la clase: Conexion
 * Version: 1.0
 * Fecha: 2/8/2018
 * Copyright: Juan Pablo Elias
 * @author Juan Pablo Elias Hernández
 */
public class Conexion
{
    private Connection conn;

    public Connection getConn() {
        return conn;
    }

    public void setConn(Connection conn) {
        this.conn = conn;
    }
    
    public void conectar()
    {
        try 
        {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/tienda", "root", "");
        } 
        catch (SQLException | ClassNotFoundException e) 
        {
            JOptionPane.showMessageDialog(null, "Error al conectar: " + e.toString(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void desconectar()
    {
        try
        {
            if (conn != null && !conn.isClosed())
            {
                conn.close();
            }
        }
        catch (SQLException e)
        {
            JOptionPane.showMessageDialog(null, "Error al desconectar: " + e.toString(), "Error", 0);
        }
    }
}
