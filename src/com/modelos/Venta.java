package com.modelos;

/**
 * Nombre de la clase: Venta
 * Version: 1.0
 * Fecha: 2/8/2018
 * Copyright: Juan Pablo Elias
 * @author Juan Pablo Elias Hernández
 */
public class Venta
{
    private int id;
    private String producto;
    private int cantidad;
    private double precio;
    private double total;

    public Venta() {
    }

    public Venta(int id, String producto, int cantidad, double precio, double total) {
        this.id = id;
        this.producto = producto;
        this.cantidad = cantidad;
        this.precio = precio;
        this.total = total;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
    
    public void calcularTotal()
    {
        this.total = precio * cantidad;
    }
}
