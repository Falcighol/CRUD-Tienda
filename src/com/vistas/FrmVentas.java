package com.vistas;

import com.dao.DaoVenta;
import com.modelos.Venta;
import com.utils.Validaciones;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 * Nombre de la clase: FrmVentas
 * Version: 1.0
 * Fecha: 2/8/2018
 * Copyright: Juan Pablo Elias
 * @author Juan Pablo Elias Hernández
 */
public class FrmVentas extends javax.swing.JFrame
{
    private Boolean adding = false;
    private Boolean editing = false;
    Validaciones val = new Validaciones();
    DaoVenta daoV = new DaoVenta();
    Venta ven = new Venta();
    
    public FrmVentas()
    {
        initComponents();
        setLocationRelativeTo(null);
        controls(false);
        all();
        buttons();
    }

    public Boolean existRows()
    {
        Boolean estado = false;
        if (jTable.getRowCount() > 0)
        {
            estado = true;
        }
        return estado;
    }
    
    public void buttons()
    {
        if (!existRows())
        {
            jBtnAdd.setEnabled(true);
            jBtnEdit.setEnabled(false);
            jBtnDelete.setEnabled(false);
            jBtnSave.setEnabled(false);
            jBtnCancelar.setEnabled(false);
        }
        else
        {
            jBtnAdd.setEnabled(true);
            jBtnEdit.setEnabled(true);
            jBtnDelete.setEnabled(true);
            jBtnSave.setEnabled(false);
            jBtnCancelar.setEnabled(false);
        }
        if (adding || editing)
        {
            jBtnAdd.setEnabled(false);
            jBtnEdit.setEnabled(false);
            jBtnDelete.setEnabled(false);
            jBtnSave.setEnabled(true);
            jBtnCancelar.setEnabled(true);
        }
    }
    
    public void controls(Boolean option)
    {
        jTxtProducto.setEnabled(option);
        jTxtPrecio.setEnabled(option);
        jTxtCantidad.setEnabled(option);
        jSptProducto.setVisible(option);
        jSptPrecio.setVisible(option);
        jSptCantidad.setVisible(option);
    }
    
    public void all()
    {
        String[] columnas = {"ID", "Producto", "Precio", "Cantidad", "Total"};
        Object[] obj = new Object[5];
        DefaultTableModel tabla = new DefaultTableModel(null, columnas);
        List ls;
        try
        {
            ls = daoV.all();
            for (int i = 0; i < ls.size(); i++)
            {
                ven = (Venta) ls.get(i);
                obj[0] = ven.getId();
                obj[1] = ven.getProducto();
                obj[2] = ven.getPrecio();
                obj[3] = ven.getCantidad();
                obj[4] = ven.getTotal();
                tabla.addRow(obj);
            }
            this.jTable.setModel(tabla);
            jTable.getColumnModel().getColumn(0).setMaxWidth(0);
            jTable.getColumnModel().getColumn(0).setMinWidth(0);
            jTable.getTableHeader().getColumnModel().getColumn(0).setMaxWidth(0);
            jTable.getTableHeader().getColumnModel().getColumn(0).setMinWidth(0);
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(null, "Error generando tabla: " + e.toString(), 
                    "Error" + e.getMessage(), JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void add()
    {
        adding = true;
        buttons();
        controls(true);
        clean();
        jTable.setEnabled(false);
    }
    
    public void edit()
    {
        if (validacion())
        {
            if (JOptionPane.showConfirmDialog(this, "¿Desea editar el registro?",
                    "Mensaje", JOptionPane.YES_NO_OPTION) == 0)
            {
                editing = true;
                buttons();
                controls(true);
                jTable.setEnabled(false);
            }
        }
        else
        {
            JOptionPane.showMessageDialog(this, "Seleccione un registro para editar.",
                    "", JOptionPane.WARNING_MESSAGE);
        }
    }
    
    public void delete()
    {
        try
        {
            if (validacion())
            {
                if (JOptionPane.showConfirmDialog(this, "¿Desea eliminar el registro?",
                        "Mensaje", JOptionPane.YES_NO_OPTION) == 0)
                {
                    ven.setId(Integer.parseInt(jTxtId.getText()));
                    daoV.delete(ven);
                    all();
                    clean();
                    JOptionPane.showMessageDialog(this, "Registro eliminado exitosamente.",
                        "Mensaje", JOptionPane.INFORMATION_MESSAGE);
                }
            }
            else
            {
                JOptionPane.showMessageDialog(this, "Seleccione un registro para eliminar.",
                        "Mensaje", JOptionPane.WARNING_MESSAGE);
            }
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(this, "Error al eliminar: " + e.toString(),
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        finally
        {
            buttons();
        }
    }
    
    public void save()
    {
        try
        {
            if (validacion())
            {
                if (adding && !editing)
                {
                    ven.setId(0);
                    ven.setProducto(jTxtProducto.getText());
                    ven.setPrecio(Double.parseDouble(jTxtPrecio.getText()));
                    ven.setCantidad(Integer.parseInt(jTxtCantidad.getText()));
                    ven.calcularTotal();
                    daoV.add(ven);
                }
                else if (!adding && editing)
                {
                    ven.setId(Integer.parseInt(jTxtId.getText()));
                    ven.setProducto(jTxtProducto.getText());
                    ven.setPrecio(Double.parseDouble(jTxtPrecio.getText()));
                    ven.setCantidad(Integer.parseInt(jTxtCantidad.getText()));
                    ven.calcularTotal();
                    daoV.edit(ven);
                }
                adding = false;
                editing = false;
                controls(false);
                jTable.setEnabled(true);
                clean();
                all();
                JOptionPane.showMessageDialog(this, "Registro guardado exitosamente.",
                            "Mensaje", JOptionPane.INFORMATION_MESSAGE);
            }
            else
            {
                JOptionPane.showMessageDialog(this, "Ingrese los datos obligatorios.",
                        "Mensaje", JOptionPane.WARNING_MESSAGE);
            }
        }
        catch (Exception e)
        {
            JOptionPane.showMessageDialog(this, "Error al guardar: " + e.toString(),
                    "Error", JOptionPane.ERROR_MESSAGE);
        }
        finally
        {
            buttons();
        }
    }
    
    public void cancel()
    {
        adding = false;
        editing = false;
        controls(false);
        buttons();
        jTable.setEnabled(true);
        clean();
        all();
    }
    
    public void clean()
    {
        jTxtId.setText("");
        jTxtProducto.setText("");
        jTxtPrecio.setText("");
        jTxtCantidad.setText("");
        jTxtTotal.setText("");
    }
    
    public Boolean validacion()
    {
        Boolean estado = true;
        if (val.IsNullOrEmpty(jTxtProducto.getText()))
        {
            estado = false;
        }
        if (val.IsNullOrEmpty(jTxtPrecio.getText()))
        {
            estado = false;
        }
        if (val.IsNullOrEmpty(jTxtCantidad.getText()))
        {
            estado = false;
        }
        return estado;
    }
    
    public void llenarCampos()
    {
        if (existRows())
        {
            if (this.jTable.getSelectedRow() > -1)
            {
                int fila = this.jTable.getSelectedRow();
                this.jTxtId.setText(String.valueOf(this.jTable.getValueAt(fila, 0)));
                this.jTxtProducto.setText(String.valueOf(this.jTable.getValueAt(fila, 1)));
                this.jTxtPrecio.setText(String.valueOf(this.jTable.getValueAt(fila, 2)));
                this.jTxtCantidad.setText(String.valueOf(this.jTable.getValueAt(fila, 3)));
                this.jTxtTotal.setText(String.valueOf(this.jTable.getValueAt(fila, 4)));
            }
        }
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPnlPrincipal = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable = new JTable(){
            public boolean isCellEditable(int rowIndex, int colIndex) {
                return false; // Celdas no editables.
            }
        };
        jPanel3 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jTxtProducto = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jTxtPrecio = new javax.swing.JTextField();
        jTxtId = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jTxtCantidad = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jTxtTotal = new javax.swing.JTextField();
        jSptProducto = new javax.swing.JSeparator();
        jSptPrecio = new javax.swing.JSeparator();
        jSptCantidad = new javax.swing.JSeparator();
        jPanel4 = new javax.swing.JPanel();
        jBtnEdit = new javax.swing.JButton();
        jBtnAdd = new javax.swing.JButton();
        jBtnCancelar = new javax.swing.JButton();
        jBtnSave = new javax.swing.JButton();
        jBtnDelete = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPnlPrincipal.setBackground(new java.awt.Color(252, 252, 252));
        jPnlPrincipal.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(0, 102, 102));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setFont(new java.awt.Font("Raleway", 0, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Gestión de ventas");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 20, -1, -1));

        jPnlPrincipal.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 690, 80));

        jPanel2.setBackground(new java.awt.Color(252, 252, 252));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)), "Productos", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Quicksand", 0, 12), new java.awt.Color(102, 102, 102))); // NOI18N
        jPanel2.setForeground(new java.awt.Color(153, 153, 153));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jScrollPane1.setBackground(new java.awt.Color(252, 252, 252));
        jScrollPane1.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));

        jTable.setBackground(new java.awt.Color(252, 252, 252));
        jTable.setFont(new java.awt.Font("Quicksand", 0, 12)); // NOI18N
        jTable.setForeground(new java.awt.Color(102, 102, 102));
        jTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ID", "Producto", "Precio", "Cantidad", "Total"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable.setAlignmentX(2.5F);
        jTable.setAlignmentY(2.5F);
        jTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_NEXT_COLUMN);
        jTable.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jTable.setGridColor(new java.awt.Color(225, 225, 225));
        jTable.setRowHeight(25);
        jTable.setRowMargin(2);
        jTable.setSelectionBackground(new java.awt.Color(102, 102, 102));
        jTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jTable.getTableHeader().setReorderingAllowed(false);
        jTable.setUpdateSelectionOnSort(false);
        jTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTable);
        if (jTable.getColumnModel().getColumnCount() > 0) {
            jTable.getColumnModel().getColumn(0).setResizable(false);
            jTable.getColumnModel().getColumn(1).setResizable(false);
            jTable.getColumnModel().getColumn(2).setResizable(false);
            jTable.getColumnModel().getColumn(3).setResizable(false);
            jTable.getColumnModel().getColumn(4).setResizable(false);
        }

        jPanel2.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 630, 130));

        jPnlPrincipal.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 300, 670, 180));

        jPanel3.setBackground(new java.awt.Color(252, 252, 252));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)), "Datos", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Quicksand", 0, 12), new java.awt.Color(102, 102, 102))); // NOI18N
        jPanel3.setForeground(new java.awt.Color(153, 153, 153));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel5.setFont(new java.awt.Font("Quicksand", 0, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(102, 102, 102));
        jLabel5.setText("Producto:");
        jPanel3.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 48, -1, -1));

        jTxtProducto.setBackground(new java.awt.Color(252, 252, 252));
        jTxtProducto.setFont(new java.awt.Font("Quicksand", 0, 13)); // NOI18N
        jTxtProducto.setForeground(new java.awt.Color(102, 102, 102));
        jTxtProducto.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtProducto.setBorder(null);
        jTxtProducto.setSelectionColor(new java.awt.Color(153, 153, 153));
        jTxtProducto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtProductoKeyTyped(evt);
            }
        });
        jPanel3.add(jTxtProducto, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 60, 190, 20));

        jLabel6.setFont(new java.awt.Font("Quicksand", 0, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(102, 102, 102));
        jLabel6.setText("Precio:");
        jPanel3.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 48, -1, -1));

        jTxtPrecio.setBackground(new java.awt.Color(252, 252, 252));
        jTxtPrecio.setFont(new java.awt.Font("Quicksand", 0, 13)); // NOI18N
        jTxtPrecio.setForeground(new java.awt.Color(102, 102, 102));
        jTxtPrecio.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtPrecio.setBorder(null);
        jTxtPrecio.setSelectionColor(new java.awt.Color(153, 153, 153));
        jTxtPrecio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtPrecioKeyTyped(evt);
            }
        });
        jPanel3.add(jTxtPrecio, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 60, 60, 20));

        jTxtId.setBackground(new java.awt.Color(252, 252, 252));
        jTxtId.setFont(new java.awt.Font("Quicksand", 0, 12)); // NOI18N
        jTxtId.setForeground(new java.awt.Color(102, 102, 102));
        jTxtId.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtId.setBorder(null);
        jTxtId.setEnabled(false);
        jTxtId.setSelectionColor(new java.awt.Color(153, 153, 153));
        jPanel3.add(jTxtId, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 20, 50, 20));

        jLabel7.setFont(new java.awt.Font("Quicksand", 0, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(102, 102, 102));
        jLabel7.setText("Cantidad:");
        jPanel3.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 48, -1, -1));

        jTxtCantidad.setBackground(new java.awt.Color(252, 252, 252));
        jTxtCantidad.setFont(new java.awt.Font("Quicksand", 0, 13)); // NOI18N
        jTxtCantidad.setForeground(new java.awt.Color(102, 102, 102));
        jTxtCantidad.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtCantidad.setBorder(null);
        jTxtCantidad.setSelectionColor(new java.awt.Color(153, 153, 153));
        jTxtCantidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtCantidadKeyTyped(evt);
            }
        });
        jPanel3.add(jTxtCantidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 60, 80, 20));

        jLabel8.setFont(new java.awt.Font("Quicksand", 0, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(102, 102, 102));
        jLabel8.setText("Total:");
        jPanel3.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 48, -1, -1));

        jTxtTotal.setBackground(new java.awt.Color(252, 252, 252));
        jTxtTotal.setFont(new java.awt.Font("Quicksand", 0, 13)); // NOI18N
        jTxtTotal.setForeground(new java.awt.Color(102, 102, 102));
        jTxtTotal.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        jTxtTotal.setBorder(null);
        jTxtTotal.setEnabled(false);
        jTxtTotal.setSelectionColor(new java.awt.Color(153, 153, 153));
        jTxtTotal.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTxtTotalKeyTyped(evt);
            }
        });
        jPanel3.add(jTxtTotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 60, 60, 20));

        jSptProducto.setBackground(new java.awt.Color(252, 252, 252));
        jSptProducto.setForeground(new java.awt.Color(102, 102, 102));
        jPanel3.add(jSptProducto, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 80, 190, 10));

        jSptPrecio.setBackground(new java.awt.Color(252, 252, 252));
        jSptPrecio.setForeground(new java.awt.Color(102, 102, 102));
        jPanel3.add(jSptPrecio, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 80, 60, 10));

        jSptCantidad.setBackground(new java.awt.Color(252, 252, 252));
        jSptCantidad.setForeground(new java.awt.Color(102, 102, 102));
        jPanel3.add(jSptCantidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 80, 80, 10));

        jPnlPrincipal.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, 670, 120));

        jPanel4.setBackground(new java.awt.Color(252, 252, 252));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)), "Acciones", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Quicksand", 0, 12), new java.awt.Color(102, 102, 102))); // NOI18N
        jPanel4.setForeground(new java.awt.Color(153, 153, 153));

        jBtnEdit.setBackground(new java.awt.Color(204, 204, 204));
        jBtnEdit.setFont(new java.awt.Font("Quicksand", 0, 14)); // NOI18N
        jBtnEdit.setForeground(new java.awt.Color(51, 51, 51));
        jBtnEdit.setText("Editar");
        jBtnEdit.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jBtnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnEditActionPerformed(evt);
            }
        });

        jBtnAdd.setBackground(new java.awt.Color(204, 204, 204));
        jBtnAdd.setFont(new java.awt.Font("Quicksand", 0, 14)); // NOI18N
        jBtnAdd.setForeground(new java.awt.Color(51, 51, 51));
        jBtnAdd.setText("Nuevo");
        jBtnAdd.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jBtnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnAddActionPerformed(evt);
            }
        });

        jBtnCancelar.setBackground(new java.awt.Color(204, 204, 204));
        jBtnCancelar.setFont(new java.awt.Font("Quicksand", 0, 14)); // NOI18N
        jBtnCancelar.setForeground(new java.awt.Color(51, 51, 51));
        jBtnCancelar.setText("Cancelar");
        jBtnCancelar.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jBtnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnCancelarActionPerformed(evt);
            }
        });

        jBtnSave.setBackground(new java.awt.Color(204, 204, 204));
        jBtnSave.setFont(new java.awt.Font("Quicksand", 0, 14)); // NOI18N
        jBtnSave.setForeground(new java.awt.Color(51, 51, 51));
        jBtnSave.setText("Guardar");
        jBtnSave.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jBtnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnSaveActionPerformed(evt);
            }
        });

        jBtnDelete.setBackground(new java.awt.Color(204, 204, 204));
        jBtnDelete.setFont(new java.awt.Font("Quicksand", 0, 14)); // NOI18N
        jBtnDelete.setForeground(new java.awt.Color(51, 51, 51));
        jBtnDelete.setText("Eliminar");
        jBtnDelete.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        jBtnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnDeleteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(39, 39, 39)
                .addComponent(jBtnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(jBtnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(jBtnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(jBtnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(jBtnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(41, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jBtnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBtnEdit, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBtnDelete, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBtnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jBtnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPnlPrincipal.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 220, 670, 70));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPnlPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 689, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPnlPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 491, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTableMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableMouseClicked
        llenarCampos();
    }//GEN-LAST:event_jTableMouseClicked

    private void jTxtProductoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtProductoKeyTyped
        val.wordsOnly(evt);
    }//GEN-LAST:event_jTxtProductoKeyTyped

    private void jTxtPrecioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtPrecioKeyTyped
        val.numbersAndPointOnly(evt, jTxtPrecio.getText());
    }//GEN-LAST:event_jTxtPrecioKeyTyped

    private void jBtnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnEditActionPerformed
        edit();
    }//GEN-LAST:event_jBtnEditActionPerformed

    private void jBtnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnAddActionPerformed
        add();
    }//GEN-LAST:event_jBtnAddActionPerformed

    private void jBtnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnCancelarActionPerformed
        cancel();
    }//GEN-LAST:event_jBtnCancelarActionPerformed

    private void jBtnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnSaveActionPerformed
        save();
    }//GEN-LAST:event_jBtnSaveActionPerformed

    private void jBtnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnDeleteActionPerformed
        delete();
    }//GEN-LAST:event_jBtnDeleteActionPerformed

    private void jTxtCantidadKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtCantidadKeyTyped
        val.numbersOnly(evt);
    }//GEN-LAST:event_jTxtCantidadKeyTyped

    private void jTxtTotalKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTxtTotalKeyTyped
        val.numbersAndPointOnly(evt, jTxtTotal.getText());
    }//GEN-LAST:event_jTxtTotalKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmVentas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmVentas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jBtnAdd;
    private javax.swing.JButton jBtnCancelar;
    private javax.swing.JButton jBtnDelete;
    private javax.swing.JButton jBtnEdit;
    private javax.swing.JButton jBtnSave;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPnlPrincipal;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSptCantidad;
    private javax.swing.JSeparator jSptPrecio;
    private javax.swing.JSeparator jSptProducto;
    private javax.swing.JTable jTable;
    private javax.swing.JTextField jTxtCantidad;
    private javax.swing.JTextField jTxtId;
    private javax.swing.JTextField jTxtPrecio;
    private javax.swing.JTextField jTxtProducto;
    private javax.swing.JTextField jTxtTotal;
    // End of variables declaration//GEN-END:variables
}
